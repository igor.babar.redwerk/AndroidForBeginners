from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice, MonkeyImage
import commands
import sys

device = MonkeyRunner.waitForConnection()

device.startActivity(component='com.couchbase.todolite/com.couchbase.todolite.LoginActivity')

MonkeyRunner.sleep(2)
device.touch(420,733,'downAndUp')
MonkeyRunner.sleep(2)
# device.touch(595,112,'downAndUp')
# MonkeyRunner.sleep(2)
# device.type('ConquestOfTheWorldList')
# MonkeyRunner.sleep(2)
# device.touch(549,746,'downAndUp')
# MonkeyRunner.sleep(4)

actual = device.takeSnapshot()
actual.writeToFile('./screenshots/actual.png','png')
expected = MonkeyRunner.loadImageFromFile('./screenshots/expected.png')
result = expected.sameAs(actual,1)
print result
if result == True:
	print 'Tested ok'
else:
	print 'Test failed'
